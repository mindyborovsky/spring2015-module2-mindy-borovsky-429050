<!DOCTYPE html>
<html>
<head>
    <title>Result</title>
    <meta charset="utf-8"/>
</head>
<body>
	<?php
	$firstnumber = $_GET['firstnumber'];
	$secondnumber = $_GET['secondnumber'];
	$operation = $_GET['operations'];

	if (is_numeric($firstnumber) && is_numeric($secondnumber)){
    	if ($operation == "Addition"){
    		printf("Hello, %f added to %f equals %f", $firstnumber, $secondnumber, (float)$firstnumber + (float)$secondnumber);
    	}
    	else if ($operation == "Subtraction"){
    		printf("Hello, %f - %f equals %f", $firstnumber, $secondnumber, (float)$firstnumber - (float)$secondnumber);
    	}
    	else if ($operation == "Multiplication"){
    		printf("Hello, %f multiplied by %f equals %f", $firstnumber, $secondnumber, (float)$firstnumber * (float)$secondnumber);
    	}
    	else if ($operation == "Division"){
    		if ($secondnumber == 0){
    			printf("Error! Division by 0.");
    		}
    		else {
    			printf("Hello, %f divided by %f equals %f", $firstnumber, $secondnumber, (float)$firstnumber / (float)$secondnumber);
    		}
    		
    	}
    	else {
    		printf("I'm sorry you did not select an operator. Please try again");
    	}
    }
    else {
    	printf("Type mismatch. Please enter valid input.");
    }

	?>
</body>
</html>